

@SuppressWarnings("serial")
public class NotEnoughBudgetException extends Exception {
	
	public NotEnoughBudgetException(String message) {
		super(message);
	}
	
	public NotEnoughBudgetException(String message, Throwable throwable) {
		super(message, throwable);
	}
	
}
