
/**
 * @(#) DishIngredient.java
 */

public class DishIngredient
{
	private String name;
	
	private float cost;
	
	private Dish Dish;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the cost
	 */
	public float getCost() {
		return cost;
	}

	/**
	 * @param cost the cost to set
	 */
	public void setCost(float cost) {
		this.cost = cost;
	}

	/**
	 * @return the dish
	 */
	public Dish getDish() {
		return Dish;
	}

	/**
	 * @param dish the dish to set
	 */
	public void setDish(Dish dish) {
		Dish = dish;
	}
	
	
	
	
}
