
/**
 * @(#) Customer.java
 */

public class Customer extends Person
{
	private String surname;
	
	private String telephone;
	
	private int number;
	
	private int taxCode;
	
	private Order Order;
	
	private Statistics Statistics;
	
	private ServiceLevelSatisfaction ServiceLevelSatisfaction;
	
	private int numberTable;

	/**
	 * @return the surname
	 */
	public String getSurname( ) {
		return surname;
	}

	/**
	 * @param surname the surname to set
	 */
	public void setSurname( String surname ) {
		this.surname = surname;
	}

	/**
	 * @return the telephone
	 */
	public String getTelephone( ) {
		return telephone;
	}

	/**
	 * @param telephone the telephone to set
	 */
	public void setTelephone( String telephone ) {
		this.telephone = telephone;
	}

	/**
	 * @return the number
	 */
	public int getNumber( ) {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber( int number ) {
		this.number = number;
	}

	/**
	 * @return the taxCode
	 */
	public int getTaxCode( ) {
		return taxCode;
	}

	/**
	 * @param taxCode the taxCode to set
	 */
	public void setTaxCode( int taxCode ) {
		this.taxCode = taxCode;
	}

	/**
	 * @return the order
	 */
	public Order getOrder( ) {
		return Order;
	}

	/**
	 * @param order the order to set
	 */
	public void setOrder( Order order ) {
		Order = order;
	}

	/**
	 * @return the statistics
	 */
	public Statistics getStatistics( ) {
		return Statistics;
	}

	/**
	 * @param statistics the statistics to set
	 */
	public void setStatistics( Statistics statistics ) {
		Statistics = statistics;
	}

	/**
	 * @return the serviceLevelSatisfaction
	 */
	public ServiceLevelSatisfaction getServiceLevelSatisfaction( ) {
		return ServiceLevelSatisfaction;
	}

	/**
	 * @param serviceLevelSatisfaction the serviceLevelSatisfaction to set
	 */
	public void setServiceLevelSatisfaction( ServiceLevelSatisfaction serviceLevelSatisfaction ) {
		ServiceLevelSatisfaction = serviceLevelSatisfaction;
	}

	/**
	 * @return the numberTable
	 */
	public int getNumberTable( ) {
		return numberTable;
	}

	/**
	 * @param numberTable the numberTable to set
	 */
	public void setNumberTable( int numberTable ) {
		this.numberTable = numberTable;
	}
	
	
	
	
}
