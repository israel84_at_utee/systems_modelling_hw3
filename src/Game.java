
import java.util.ArrayList;
import java.util.Scanner;
import java.util.List;

/**
 * @(#) Game.java
 */

public class Game
{
	private Boolean started;
	
	private Boolean finished;
	
	private Player Player;
	
	private RankingList RankingList;
	
	private int playingDays;
	
	public Game(){
		
	}
	
	
	/**
	 * @param started
	 * @param finished
	 * @param player
	 * @param rankingList
	 * @param playingDays
	 */
	public Game(Boolean started, Boolean finished, Player player,
			RankingList rankingList) {
		super();
		this.started = started;
		this.finished = finished;
		Player = player;
		RankingList = rankingList;
		this.playingDays = 1;
	}

	public void startGame()
	{
		this.started = true;
	}
	
	public void endGame() 
	{

		this.finished = true;
		System.out.println("Hello, Welcome to the game!!!");
		Scanner user_input = new Scanner(System.in);
		
		System.out.println("Enter your name: ");
		String playerName = user_input.next();
		
		System.out.println("Enter the restaraunt name:");
		String restarauntName = user_input.next();
		
		System.out.println("Enter the restaraunt adress:");
		String restarauntAddress = user_input.next();
		
		System.out.println("Enter the city name:");
		String cityName = user_input.next();
		
		Chef chef = new Chef();
		Barman barman = new Barman();
		List<Waiter> waiters = new ArrayList<Waiter>();
		waiters.add(new Waiter(false, true, "Johnny"));
		waiters.add(new Waiter(false, true, "Scotty"));
		waiters.add(new Waiter(false, true, "Jimmy"));
		
		List<Supplier> provider =  new ArrayList<Supplier>();
		
		List<Table> tables = new ArrayList<Table>();
		
		for(int i=1; i<10; i++){
			tables.add(new Table(i, null, true));
		}
		
		Restaurant res = new Restaurant(restarauntName, restarauntAddress, cityName, null, chef, barman, 
										waiters, provider, tables);
		
		int budget = 10000;
		
		Player = new Player();
		Player.setName(playerName);
	}
	
	
	public boolean playerWins( )
	{
		if( playingDays >= 30 && Player.getRestaurant().getBudget() > 0 ) {
			finished = true;
		} else {
			finished = false;
		}
		return getFinished();
	}
	
	public void calculateScore( )
	{
		Player.setScore(Player.getRestaurant().getBudget());
	}
	
	public List<Integer> generateClientStatistics( )
	{
		List<Integer> statistics = new ArrayList<Integer>();
		
		for(Table table : Player.getRestaurant().getTables() ) {
			for(Customer customer : table.getCustomers()) {
				statistics.add(customer.getStatistics().getAmountMoneySpent());
				statistics.add(customer.getStatistics().getAverageCalories());
				statistics.add(customer.getStatistics().getAverageVolume());
				statistics.add(customer.getStatistics().getNumBeverages());
				statistics.add(customer.getStatistics().getNumDishes());
			}
		}
		
		return statistics;
	}
	
	public List<String> generateGameResults( )
	{
		List<String> gameResults = new ArrayList<String>();
		
		Player.setScore(Player.getRestaurant().getBudget());
		
		gameResults.add(Player.getScore()+"");
		
		List<AdditionalCosts> ac = Player.getRestaurant().getAdditionalCosts();
		
		double sumAdditionalCosts = 0;
		
		for(AdditionalCosts aacc : ac) {
			sumAdditionalCosts = sumAdditionalCosts + aacc.getAmmount();
		}
		
		gameResults.add( sumAdditionalCosts+"" );
		
		return gameResults;
	}
	
	public boolean playerLosses( )
	{
		if( Player.getRestaurant().getBudget() <= 0 )  {
			finished = true;
		} else {
			finished = false;
		}
		
		return getFinished();
	}
	
	public boolean gameExpires( )
	{
		if( playingDays >= 30 ){
			finished = true;
		} else {
			finished = false;
		}
		
		return getFinished();
	}
	
	public void playerDecidesToFinish( )
	{
		this.finished = true;
		Player.setScore(Player.getRestaurant().getBudget());
		
		List<Integer> cs = generateClientStatistics();
		
		List<String> gr = generateGameResults();
		
		//here we should print those results.
	}

	/**
	 * @return the started
	 */
	public Boolean getStarted() {
		return started;
	}

	/**
	 * @param started the started to set
	 */
	public void setStarted(Boolean started) {
		this.started = started;
	}

	/**
	 * @return the finished
	 */
	public Boolean getFinished() {
		return finished;
	}

	/**
	 * @param finished the finished to set
	 */
	public void setFinished(Boolean finished) {
		this.finished = finished;
	}

	/**
	 * @return the player
	 */
	public Player getPlayer() {
		return Player;
	}

	/**
	 * @param player the player to set
	 */
	public void setPlayer(Player player) {
		Player = player;
	}

	/**
	 * @return the rankingList
	 */
	public RankingList getRankingList() {
		return RankingList;
	}

	/**
	 * @param rankingList the rankingList to set
	 */
	public void setRankingList(RankingList rankingList) {
		RankingList = rankingList;
	}
	
	
	
	public void budgetLessThanZero( )
	{
		if( Player.getRestaurant().getBudget() <= 0 ) {
			finished = true;
		}
	}

	/**
	 * @return the playingDays
	 */
	public int getPlayingDays() {
		return playingDays;
	}

	/**
	 * @param playingDays the playingDays to set
	 */
	public void setPlayingDays(int playingDays) {
		this.playingDays = playingDays;
	}	
	
}
