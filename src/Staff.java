
/**
 * @(#) Staff.java
 */

public class Staff extends Person
{
	private String surname;
	
	private Experience experience;
	
	private float weekSalary;
	
	public int trainingResult;
	
	public void takeTrainingCourse( )
	{
		int min = 4;
		int max = 10;
		
		setTrainingResult( min + (int)(Math.random() * ((max - min) + 1)) );
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @param surname the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * @return the experience
	 */
	public Experience getExperience() {
		return experience;
	}

	/**
	 * @param experience the experience to set
	 */
	public void setExperience(Experience experience) {
		this.experience = experience;
	}

	/**
	 * @return the weekSalary
	 */
	public float getWeekSalary() {
		return weekSalary;
	}

	/**
	 * @param weekSalary the weekSalary to set
	 */
	public void setWeekSalary(float weekSalary) {
		this.weekSalary = weekSalary;
	}

	/**
	 * @return the trainingResult
	 */
	public int getTrainingResult() {
		return trainingResult;
	}

	/**
	 * @param trainingResult the trainingResult to set
	 */
	public void setTrainingResult(int trainingResult) {
		this.trainingResult = trainingResult;
	}
	
}
