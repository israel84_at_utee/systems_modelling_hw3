
/**
 * @(#) AdditionalCosts.java
 */

public class AdditionalCosts
{
	private Restaurant Restaurant;
	
	private String name;
	
	private float ammount;
	
	private String desciption;

	/**
	 * @return the restaurant
	 */
	public Restaurant getRestaurant() {
		return Restaurant;
	}

	/**
	 * @param restaurant the restaurant to set
	 */
	public void setRestaurant(Restaurant restaurant) {
		Restaurant = restaurant;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the ammount
	 */
	public float getAmmount() {
		return ammount;
	}

	/**
	 * @param ammount the ammount to set
	 */
	public void setAmmount(float ammount) {
		this.ammount = ammount;
	}

	/**
	 * @return the desciption
	 */
	public String getDesciption() {
		return desciption;
	}

	/**
	 * @param desciption the desciption to set
	 */
	public void setDesciption(String desciption) {
		this.desciption = desciption;
	}
	

	
	
}
