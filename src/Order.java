
/**
 * @(#) Order.java
 */

public class Order
{
	private Customer Customer;
	
	private Dish Dish;
	
	private Beverage Beverage;
	
	private int number;

	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		return Customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Customer customer) {
		Customer = customer;
	}

	/**
	 * @return the dish
	 */
	public Dish getDish() {
		return Dish;
	}

	/**
	 * @param dish the dish to set
	 */
	public void setDish(Dish dish) {
		Dish = dish;
	}

	/**
	 * @return the beverage
	 */
	public Beverage getBeverage() {
		return Beverage;
	}

	/**
	 * @param beverage the beverage to set
	 */
	public void setBeverage(Beverage beverage) {
		Beverage = beverage;
	}

	/**
	 * @return the number
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(int number) {
		this.number = number;
	}
	
	
}
