
/**
 * @(#) Dish.java
 */

public class Dish
{
	private String name;
	
	public int caloriesCount;
	
	private Menu Menu;
	
	private float cost;
	
	private Order Order;
	
	private Quality quality;
	
	private DishIngredient DishIngredient;
	
	private int isSet;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the caloriesCount
	 */
	public int getCaloriesCount() {
		return caloriesCount;
	}

	/**
	 * @param caloriesCount the caloriesCount to set
	 */
	public void setCaloriesCount(int caloriesCount) {
		this.caloriesCount = caloriesCount;
	}

	/**
	 * @return the menu
	 */
	public Menu getMenu() {
		return Menu;
	}

	/**
	 * @param menu the menu to set
	 */
	public void setMenu(Menu menu) {
		Menu = menu;
	}

	/**
	 * @return the cost
	 */
	public float getCost() {
		return cost;
	}

	/**
	 * @param cost the cost to set
	 */
	public void setCost(float cost) {
		this.cost = cost;
	}

	/**
	 * @return the order
	 */
	public Order getOrder() {
		return Order;
	}

	/**
	 * @param order the order to set
	 */
	public void setOrder(Order order) {
		Order = order;
	}

	/**
	 * @return the quality
	 */
	public Quality getQuality() {
		return quality;
	}

	/**
	 * @param quality the quality to set
	 */
	public void setQuality(Quality quality) {
		this.quality = quality;
	}

	/**
	 * @return the dishIngredient
	 */
	public DishIngredient getDishIngredient() {
		return DishIngredient;
	}

	/**
	 * @param dishIngredient the dishIngredient to set
	 */
	public void setDishIngredient(DishIngredient dishIngredient) {
		DishIngredient = dishIngredient;
	}

	/**
	 * @return the isSet
	 */
	public int getIsSet() {
		return isSet;
	}

	/**
	 * @param isSet the isSet to set
	 */
	public void setIsSet(int isSet) {
		this.isSet = isSet;
	}
	
	
	
	
}
