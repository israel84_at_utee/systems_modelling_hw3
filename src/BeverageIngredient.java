
/**
 * @(#) BeverageIngredient.java
 */

public class BeverageIngredient
{
	private String name;
	
	private float cost;
	
	private Beverage Beverage;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the cost
	 */
	public float getCost() {
		return cost;
	}

	/**
	 * @param cost the cost to set
	 */
	public void setCost(float cost) {
		this.cost = cost;
	}

	/**
	 * @return the beverage
	 */
	public Beverage getBeverage() {
		return Beverage;
	}

	/**
	 * @param beverage the beverage to set
	 */
	public void setBeverage(Beverage beverage) {
		Beverage = beverage;
	}
	
	
}
