
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @(#) Menu.java
 */

public class Menu
{
	private Dish Dish;
	
	private Beverage Beverage;
	
	private int number;
	
	private boolean isSet;
	
	public void setMenu()
	{
		int dishLow, dishHigh;
		int bevLow, bevHigh;
		System.out.println("Choose the number of dishes of high and low quality (enter data, press intro, then repeat): ");
		Scanner user_input = new Scanner(System.in);
		dishHigh = user_input.nextInt();
		dishLow = user_input.nextInt();
		
		List<Beverage> bevList = new ArrayList<Beverage>();
		List<Dish> dishList = new ArrayList<Dish>();
		
		String dishName, quality;
		float cost, volume;
		int calorieCount;
				
		for(int i = 0; i < 5; i++) {
			Dish dish = new Dish(); 
			System.out.println("Enter Dish Name: ");
			dishName = user_input.next();
			dish.setName(dishName);
			System.out.println("Enter Dish Quality (Low or High): ");
			quality = user_input.next();
			if(quality == "Low")
			{
				dish.setQuality(Quality.LOW);
			}
			else if(quality == "High")
			{
				dish.setQuality(Quality.HIGH);
			}
			System.out.println("Enter Dish Cost: ");
			cost = Float.parseFloat(user_input.next());
			dish.setCost(cost);
			System.out.println("Enter Dish Calorie Count: ");
			calorieCount = Integer.parseInt(user_input.next());
			dish.setCaloriesCount(calorieCount);
			dishList.add(dish);
		}
		
		System.out.println("Choose the number of beverages of high and low quality: ");
		bevHigh = user_input.nextInt();
		bevLow = user_input.nextInt();
				
		for(int i = 0; i < 5; i++) {
			Beverage bev = new Beverage(); 
			System.out.println("Enter Dish Name: ");
			dishName = user_input.next();
			bev.setName(dishName);
			System.out.println("Enter Dish Quality (Low or High): ");
			quality = user_input.next();
			if(quality == "Low")
			{
				bev.setQuality(Quality.LOW);
			}
			else if(quality == "High")
			{
				bev.setQuality(Quality.HIGH);
			}
			System.out.println("Enter Dish Cost: ");
			cost = Float.parseFloat(user_input.next());
			bev.setCost(cost);
			System.out.println("Enter Dish Calorie Count: ");
			volume = Float.parseFloat(user_input.next());
			bev.setVolume(volume);
			bevList.add(bev);
		}
	}
	
	/**
	 * @return the dish
	 */
	public Dish getDish() {
		return Dish;
	}



	/**
	 * @param dish the dish to set
	 */
	public void setDish(Dish dish) {
		Dish = dish;
	}



	/**
	 * @return the beverage
	 */
	public Beverage getBeverage() {
		return Beverage;
	}



	/**
	 * @param beverage the beverage to set
	 */
	public void setBeverage(Beverage beverage) {
		Beverage = beverage;
	}



	/**
	 * @return the number
	 */
	public int getNumber() {
		return number;
	}



	/**
	 * @param number the number to set
	 */
	public void setNumber(int number) {
		this.number = number;
	}



	/**
	 * @return the isSet
	 */
	public boolean isSet() {
		return isSet;
	}



	/**
	 * @param isSet the isSet to set
	 */
	public void setSet(boolean isSet) {
		this.isSet = isSet;
	}
	
	
}
