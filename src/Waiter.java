
/**
 * @(#) Waiter.java
 */

import java.util.List;

public class Waiter extends Staff
{
	
	public boolean isBusy;
	public boolean isReady;
	private Table Table;
	public String waiterName;
	
	public Waiter(){
		isBusy = false;
		isReady = false;
		//assignedTables = null;
		this.waiterName = "";
	}
	
	
	public Waiter(boolean isBusy, boolean isReady, List<Table> assignedTables, String waiterName) {
		super.setName(waiterName);
		this.isBusy = isBusy;
		this.isReady = isReady;
		//this.assignedTables = assignedTables;
		this.waiterName = waiterName;
	}
	
	public Waiter(boolean isBusy, boolean isReady, String waiterName) {
		super.setName(waiterName);
		this.isBusy = isBusy;
		this.isReady = isReady;
		//this.assignedTables = assignedTables;
		this.waiterName = waiterName;
	}
	
	
	
	public boolean checkWaiter()
	{
		return this.isReady;
	}


	/**
	 * @return the assignedTables
	 */
//	public List<Table> getAssignedTables() {
//		return assignedTables;
//	}

	/**
	 * @param assignedTables the assignedTables to set
	 */
//	public void setAssignedTables(List<Table> assignedTables) {
//		this.assignedTables = assignedTables;
//	}
	
	public void assigneTable(Table table) {
		this.Table = table;
	}
	
}
