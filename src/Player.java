//
//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Scanner;


/**
 * @(#) Player.java
 */

public class Player extends Person
{
	private double score;
	
	private Game Game;
	
	private RankingList RankingList;
	
	private int number;
	
	private Restaurant restaurant;
	
	private Menu Menu;
	
	
	/**
	 * @return the score
	 */
	public double getScore() {
		return score;
	}

	/**
	 * @param score the score to set
	 */
	public void setScore(double score) {
		this.score = score;
	}

	/**
	 * @return the game
	 */
	public Game getGame() {
		return Game;
	}

	/**
	 * @param game the game to set
	 */
	public void setGame(Game game) {
		Game = game;
	}

	/**
	 * @return the rankingList
	 */
	public RankingList getRankingList() {
		return RankingList;
	}

	/**
	 * @param rankingList the rankingList to set
	 */
	public void setRankingList(RankingList rankingList) {
		RankingList = rankingList;
	}

	/**
	 * @return the number
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(int number) {
		this.number = number;
	}

	/**
	 * @return the restaurant
	 */
	public Restaurant getRestaurant() {
		return restaurant;
	}

	/**
	 * @param restaurant the restaurant to set
	 */
	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	/**
	 * @return the menu
	 */
	public Menu getMenu() {
		return Menu;
	}

	/**
	 * @param menu the menu to set
	 */
	public void setMenu(Menu menu) {
		Menu = menu;
	}
	

	public void increaseExperienceOf(Staff staffmember){

		try {
			payStaffTrainingCourse(staffmember);
			
			staffmember.takeTrainingCourse();
			
			switch ( staffmember.getTrainingResult() )	{
				case 4:
				case 5:
				case 6:
						staffmember.setExperience(Experience.LOW);
						
						if( staffmember instanceof Waiter)
							staffmember.setWeekSalary(200.0f);
						else
							staffmember.setWeekSalary(300.0f);
						
						System.out.println("The training result for " + staffmember.getName() + ", was not satisfactory, therefore it was not possible to increase his/her level of Experience.");						
						System.out.println("His/her current week salary is: " + staffmember.getWeekSalary());
					break;
			
				case 7:
				case 8:
						staffmember.setExperience(Experience.MEDIUM);
						
						if( staffmember instanceof Waiter)
							staffmember.setWeekSalary(300.0f);
						else
							staffmember.setWeekSalary(400.0f);
						
						System.out.println("The training result for " + staffmember.getName() + ", was Satisfactory, therefore now his/her level of Experience is => " + staffmember.getExperience());
						System.out.println("Now his/her current week salary is: " + staffmember.getWeekSalary());
					break;
					
				case 9:
				case 10:
						staffmember.setExperience(Experience.HIGH);
						
						if( staffmember instanceof Waiter)
							staffmember.setWeekSalary(400.0f);
						else
							staffmember.setWeekSalary(500.0f);
						
						System.out.println("The training result for " + staffmember.getName() + ", was Satisfactory, therefore now his/her level of Experience is => " + staffmember.getExperience());
						System.out.println("Now his/her current week salary is: " + staffmember.getWeekSalary());
					break;
			}
		} catch (NotEnoughBudgetException e) {
			e.printStackTrace();
		}
		
	}

	public void payStaffTrainingCourse(Staff staffmember) throws NotEnoughBudgetException { 
		
		double currentBudget = restaurant.getBudget();
		
		if( currentBudget > 0 ) {
			if(staffmember instanceof Waiter) {
				restaurant.setBudget(currentBudget - 800);
			} else if( staffmember instanceof Chef || staffmember instanceof Barman ) {
				restaurant.setBudget(currentBudget - 1200);
			}			
		} else {
			throw new NotEnoughBudgetException("[Not enough budget]");
		}
		
	}
	
}
