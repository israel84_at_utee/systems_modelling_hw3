
/**
 * @(#) Table.java
 */

public class Table
{
	private int number;
	
	private Waiter Waiter;
	
	private boolean isAvaliable;

	
	private Customer Customers[] = new Customer[2];	
	
	/**
	 * @param number
	 * @param waiter
	 * @param isAvaliable
	 * @param customer
	 */
	public Table(int number, Waiter waiter, boolean isAvaliable, Customer customers[]) {
		super();
		this.number = number;
		Waiter = waiter;
		this.isAvaliable = isAvaliable;
		//Customer = new Customer[2];
		this.Customers = customers;
	}

	public Table(int number) {
		this.number = number;
	}

	public Table(int number, Waiter waiter, boolean isAvaliable) {
		super();
		this.number = number;
		this.Waiter = waiter;
		this.isAvaliable = isAvaliable;
	}



	/**
	 * @return the number
	 */
	public int getNumber( )
	{
		return number;
	}
	
	/**
	 * @param number the number to set
	 */
	public void setNumber( int number )
	{
		this.number = number;
	}
	
	/**
	 * @return the waiter
	 */
	public Waiter getWaiter( )
	{
		return Waiter;
	}
	
	/**
	 * @param waiter the waiter to set
	 */
	public void setWaiter( Waiter waiter )
	{
		this.Waiter = waiter;
	}
	
	/**
	 * @return the isAvaliable
	 */
	public boolean isAvaliable( )
	{
		return isAvaliable;
	}
	
	/**
	 * @param isAvaliable the isAvaliable to set
	 */
	public void setAvaliable( boolean isAvaliable )
	{
		this.isAvaliable = isAvaliable;
	}

	/**
	 * @return the customer
	 */
	public Customer[] getCustomers() {
		return Customers;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomers(Customer[] customers) {
		Customers = customers;
	}
		
}
