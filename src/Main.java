//System.out.println();
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Welcome to the Owner Restaurant Game...");
		
		Player player = new Player();
		
			System.out.println("Let's get started, we need to define your data.\nPlease write your name -> ");
		player.setName(in.next());
		
			System.out.println("Hello " + player.getName() + "!.");
		player.setScore(0);
		
		Restaurant restaurant = new Restaurant();
		String aux;
		
			System.out.println("A restaurant have been created for you, we need to set some data ...\nPlease introduce the name for your Restaurant -> ");
			aux = in.next();
			restaurant.setName(aux);
		
			System.out.println("Now please write the address for your restaurant -> ");
			aux = in.next();
			restaurant.setAddress(aux);
		
			System.out.println("Now please write the city for your restaurant -> ");
			aux = in.next();
			restaurant.setCity(aux);
		
			System.out.println("You're lucky because your initial restaurant budget is: " + restaurant.getBudget() + " Euros. \nBy now you don't have additional costs.");
			restaurant.setAdditionalCosts(null);
		
		
		System.out.println("Now we need a Chef, a Barman and three waiters, let's create them.\n");
		
		Barman barman = new Barman();
		
			System.out.println("Write the name for your Barman -> ");
			aux = in.next();
			barman.setName(aux);
		
			System.out.println("Write the surname for your Barman -> ");
			aux = in.next();
			barman.setSurname(aux);
		
		
		barman.setExperience(Experience.LOW);
		barman.setWeekSalary(300.0f);
		
		Chef chef = new Chef();
		
			System.out.println("Write the name for your Chef -> ");
			aux = in.next();
		 	chef.setName(aux);
		
			System.out.println("Write the surname for your Chef -> ");
			aux = in.next();
		 	chef.setSurname(aux);
		
		 	chef.setExperience(Experience.LOW);
		 	chef.setWeekSalary(300.0f);
		
		ArrayList<Waiter> waiters = new ArrayList<Waiter>();
			waiters.add(new Waiter(false, true, "Johnny"));
			waiters.add(new Waiter(false, true, "Scotty"));
			waiters.add(new Waiter(false, true, "Sammy"));

			System.out.println("Meet your waiters: ");
			
		for(Waiter waiter:waiters){
			System.out.println("Waiter: " + waiter.getName());
		}
		
		
		restaurant.setOwnerName(player.getName());
		restaurant.setChef(chef);
		restaurant.setBarman(barman);
		restaurant.setWaiters(waiters);
		
		
		
		Game game = new Game();
		
		game.setPlayer(player);
		game.setStarted(true);
		game.setPlayingDays(1);
		
		System.out.println("Now our game is set, waiters need to know which tables are they serving to.");
		
		List<Table> tables = new ArrayList<Table>();
		
		for(int i=1; i<10; i++){
			tables.add(new Table(i, null, true));
		}
		
		//set tables to waiters.
		restaurant.assignTables(waiters, tables);
		
		System.out.println("Now your waiters have their assigned tables.");
		
		Menu menu = new Menu();
		System.out.println("Now you need to create your menus...");
		menu.setMenu();
		System.out.println("Now you menus are set.");
		
		
		System.out.println("Now you're ready to play... ");
		int playerResponse;
		int playerAuxResponse;
		int waitertotrain;
		boolean gaming = true;
		
		try {
			while (gaming) {
				
				actionsMenu();
				
				System.out.println(" Choose and option -> ");
				playerResponse = Integer.parseInt(in.next());
				
				switch(playerResponse) {
				case 1: 
					
						System.out.println("**********************************************");
						System.out.println("*       Choose the staff member              *");
						System.out.println("**********************************************");
						System.out.println("* 11 -> Waiter                               *");
						System.out.println("* 12 -> Chef                                 *");
						System.out.println("* 13 -> Barman                               *");
						System.out.println("**********************************************\n");	
						System.out.println("Choose an option -> ");
						playerAuxResponse = Integer.parseInt(in.next());
						
						if( playerAuxResponse == 11 ) {
							
							System.out.println("Which waiter do you want to train (1/2/3) ? :> ");
							waitertotrain = Integer.parseInt(in.next());
							
							if( playerAuxResponse == 1 ) {
								player.increaseExperienceOf(waiters.get(0));
							} else if ( playerAuxResponse == 2 ) {
								player.increaseExperienceOf(waiters.get(1));
							} else {
								player.increaseExperienceOf(waiters.get(2));
							}
							
						} else if ( playerAuxResponse == 12 ) {
							player.increaseExperienceOf(chef);
						} else if ( playerAuxResponse == 13 ) {
							player.increaseExperienceOf(barman);
						} else {
							System.out.println("Invalid option.");
						}
						
						
					break;
				
				case 3:
						System.out.println("\t\tYour current budget is -> " + player.getRestaurant().getBudget());
					break;
					
				case 4:
					gaming=false;
					
					break;
				}
				
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		
	  System.out.println("End of the game.");
	}
	
	public static void actionsMenu() {
		System.out.println("**********************************************");
		System.out.println("*            Actions for Player              *");
		System.out.println("**********************************************");
		System.out.println("* 1.- Trainning Staff member.                *");
		System.out.println("* 2.- See Budget.                            *");
		//System.out.println("* 3.- Rest of operation would be here.       *");
		System.out.println("* 4.- End Game.                              *");
		System.out.println("**********************************************\n");		
	}

}














