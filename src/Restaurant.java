import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

/**
 * @(#) Restaurant.java
 */

public class Restaurant
{
	private String ownerName;
	
	private String name;
	
	private String address;
	
	private String city;
	
	private double budget;
	
	private List<AdditionalCosts> additionalCosts;
	
	private Chef Chef;
	
	private Barman Barman;
	
	private List<Waiter> Waiters;
	
	private List<Supplier> Providers;
	
	private List<Table> Tables;
	
	

	public Restaurant() {
		super();
		this.ownerName = "NotSet";
		this.name = "NotSet";
		this.address = "NotSet";
		this.city = "NotSet";
		this.budget = 10000;
		this.additionalCosts = null;
		Chef = null;
		Barman = null;
		Waiters = null;
		Providers = null;
		Tables = null;
	}	

	
	/**
	 * @param name
	 * @param address
	 * @param city
	 * @param budget
	 * @param additionalCosts
	 * @param chef
	 * @param barman
	 * @param waiters
	 * @param providers
	 * @param tables
	 */
	public Restaurant(String name, String address, String city, 
			List<AdditionalCosts> additionalCosts, Chef chef, Barman barman,
			List<Waiter> waiters, List<Supplier> providers, List<Table> tables) {
		super();
		this.ownerName = "NotSet";
		this.name = name;
		this.address = address;
		this.city = city;
		this.budget = 10000;
		this.additionalCosts = additionalCosts;
		Chef = chef;
		Barman = barman;
		Waiters = waiters;
		Providers = providers;
		Tables = tables;
	}


	
	public List<Table> findFreeTables( )
	{
		List<Table> freeTables = new ArrayList<Table>();
		
		for(Table table: Tables) {
			if(table.isAvaliable() == true) {
				freeTables.add(table);
			}
		}
		
		return freeTables;
	}
	
	public void assignTables(List<Waiter> waiters, List<Table> tables) {
		  
		for (Waiter waiter : waiters) {
			//System.out.println("Assign table to the waiter: ");
			//Scanner user_input = new Scanner(System.in);
			//int num = user_input.nextInt();
			for(int i = 0; i < 3; i++) {
				waiter.assigneTable(tables.get(i));
				tables.remove(i);
			}
		}
		
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the budget
	 */
	public double getBudget() {
		return budget;
	}

	/**
	 * @param budget the budget to set
	 */
	public void setBudget(double budget) {
		this.budget = budget;
	}

	/**
	 * @return the additionalCosts
	 */
	public List<AdditionalCosts> getAdditionalCosts() {
		return additionalCosts;
	}

	/**
	 * @param additionalCosts the additionalCosts to set
	 */
	public void setAdditionalCosts(List<AdditionalCosts> additionalCosts) {
		this.additionalCosts = additionalCosts;
	}

	/**
	 * @return the chef
	 */
	public Chef getChef() {
		return Chef;
	}

	/**
	 * @param chef the chef to set
	 */
	public void setChef(Chef chef) {
		Chef = chef;
	}

	/**
	 * @return the barman
	 */
	public Barman getBarman() {
		return Barman;
	}

	/**
	 * @param barman the barman to set
	 */
	public void setBarman(Barman barman) {
		Barman = barman;
	}

	/**
	 * @return the waiter
	 */
	public List<Waiter> getWaiters() {
		return Waiters;
	}

	/**
	 * @param waiter the waiter to set
	 */
	public void setWaiters(List<Waiter> waiters) {
		this.Waiters = waiters;
	}

	/**
	 * @return the provider
	 */
	public List<Supplier> getProviders() {
		return Providers;
	}

	/**
	 * @param provider the provider to set
	 */
	public void setProviders(List<Supplier> provider) {
		this.Providers = provider;
	}

	/**
	 * @return the table
	 */
	public List<Table> getTables() {
		return Tables;
	}

	/**
	 * @param table the table to set
	 */
	public void setTables(List<Table> tables) {
		this.Tables = tables;
	}


	/**
	 * @return the ownerName
	 */
	public String getOwnerName() {
		return ownerName;
	}


	/**
	 * @param ownerName the ownerName to set
	 */
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
		
}
