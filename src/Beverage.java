
/**
 * @(#) Beverage.java
 */

public class Beverage
{
	private float cost;
	
	private String name;
	
	private float volume;
	
	private Menu Menu;
	
	private Order Order;
	
	private Quality quality;
	
	private BeverageIngredient BeverageIngredient;
	
	private int isSet;

	/**
	 * @return the cost
	 */
	public float getCost() {
		return cost;
	}

	/**
	 * @param cost the cost to set
	 */
	public void setCost(float cost) {
		this.cost = cost;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the volume
	 */
	public float getVolume() {
		return volume;
	}

	/**
	 * @param volume the volume to set
	 */
	public void setVolume(float volume) {
		this.volume = volume;
	}

	/**
	 * @return the menu
	 */
	public Menu getMenu() {
		return Menu;
	}

	/**
	 * @param menu the menu to set
	 */
	public void setMenu(Menu menu) {
		Menu = menu;
	}

	/**
	 * @return the order
	 */
	public Order getOrder() {
		return Order;
	}

	/**
	 * @param order the order to set
	 */
	public void setOrder(Order order) {
		Order = order;
	}

	/**
	 * @return the quality
	 */
	public Quality getQuality() {
		return quality;
	}

	/**
	 * @param quality the quality to set
	 */
	public void setQuality(Quality quality) {
		this.quality = quality;
	}

	/**
	 * @return the beverageIngredient
	 */
	public BeverageIngredient getBeverageIngredient() {
		return BeverageIngredient;
	}

	/**
	 * @param beverageIngredient the beverageIngredient to set
	 */
	public void setBeverageIngredient(BeverageIngredient beverageIngredient) {
		BeverageIngredient = beverageIngredient;
	}

	/**
	 * @return the isSet
	 */
	public int getIsSet() {
		return isSet;
	}

	/**
	 * @param isSet the isSet to set
	 */
	public void setIsSet(int isSet) {
		this.isSet = isSet;
	}
	

	
	
	
}
