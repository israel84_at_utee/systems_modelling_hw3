
/**
 * @(#) Statistics.java
 */

public class Statistics
{
	private int numBeverages;
	
	private int numDishes;
	
	private int averageCalories;
	
	private int averageVolume;
	
	private int amountMoneySpent;

	/**
	 * @return the numBeverages
	 */
	public int getNumBeverages() {
		return numBeverages;
	}

	/**
	 * @param numBeverages the numBeverages to set
	 */
	public void setNumBeverages(int numBeverages) {
		this.numBeverages = numBeverages;
	}

	/**
	 * @return the numDishes
	 */
	public int getNumDishes() {
		return numDishes;
	}

	/**
	 * @param numDishes the numDishes to set
	 */
	public void setNumDishes(int numDishes) {
		this.numDishes = numDishes;
	}

	/**
	 * @return the averageCalories
	 */
	public int getAverageCalories() {
		return averageCalories;
	}

	/**
	 * @param averageCalories the averageCalories to set
	 */
	public void setAverageCalories(int averageCalories) {
		this.averageCalories = averageCalories;
	}

	/**
	 * @return the averageVolume
	 */
	public int getAverageVolume() {
		return averageVolume;
	}

	/**
	 * @param averageVolume the averageVolume to set
	 */
	public void setAverageVolume(int averageVolume) {
		this.averageVolume = averageVolume;
	}

	/**
	 * @return the amountMoneySpent
	 */
	public int getAmountMoneySpent() {
		return amountMoneySpent;
	}

	/**
	 * @param amountMoneySpent the amountMoneySpent to set
	 */
	public void setAmountMoneySpent(int amountMoneySpent) {
		this.amountMoneySpent = amountMoneySpent;
	}
		
}
