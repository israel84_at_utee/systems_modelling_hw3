
/**
 * @(#) ServiceLevelSatisfaction.java
 */

public class ServiceLevelSatisfaction
{
	private boolean satisfied;
	
	private int serviceSatisfaction;
	
	private int foodSatisfaction;
	
	private int beverageSatisfaction;

	/**
	 * @return the satisfied
	 */
	public boolean isSatisfied() {
		return satisfied;
	}

	/**
	 * @param satisfied the satisfied to set
	 */
	public void setSatisfied(boolean satisfied) {
		this.satisfied = satisfied;
	}

	/**
	 * @return the serviceSatisfaction
	 */
	public int getServiceSatisfaction() {
		return serviceSatisfaction;
	}

	/**
	 * @param serviceSatisfaction the serviceSatisfaction to set
	 */
	public void setServiceSatisfaction(int serviceSatisfaction) {
		this.serviceSatisfaction = serviceSatisfaction;
	}

	/**
	 * @return the foodSatisfaction
	 */
	public int getFoodSatisfaction() {
		return foodSatisfaction;
	}

	/**
	 * @param foodSatisfaction the foodSatisfaction to set
	 */
	public void setFoodSatisfaction(int foodSatisfaction) {
		this.foodSatisfaction = foodSatisfaction;
	}

	/**
	 * @return the beverageSatisfaction
	 */
	public int getBeverageSatisfaction() {
		return beverageSatisfaction;
	}

	/**
	 * @param beverageSatisfaction the beverageSatisfaction to set
	 */
	public void setBeverageSatisfaction(int beverageSatisfaction) {
		this.beverageSatisfaction = beverageSatisfaction;
	}
		
}
