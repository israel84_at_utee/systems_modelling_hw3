
/**
 * @(#) Supplier.java
 */

public class Supplier
{
	private int name;

	/**
	 * @return the name
	 */
	public int getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(int name) {
		this.name = name;
	}	
	
}
